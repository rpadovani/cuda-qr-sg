# system dependent flags for CUDA
# CUDA sdk path
CUDA_SDK?=/usr/local/cuda/sdk
# CUDA path
CUDA_PATH?=/usr/local/cuda

# C compiler
CC = g++
CFLAGS  	= -Wall -O3 -fomit-frame-pointer -funroll-loops

# CUDA compiler
NVCC = nvcc
NVCCFLAGS = -ccbin /usr/bin/g++ \
	    -I$(CUDA_SDK)/common/inc \
	    -I$(CUDA_PATH)/include -arch=sm_20 -m64

# linker and linker options
LD = g++
LFLAGS = -L$(CUDA_PATH)/lib64 -lcuda -lcudart

PROJ = merge
.PHONY: clean

all: $(PROJ)

# CUDA source
%.o: %.cu
	@echo CUDA compiling $@
	$(NVCC) -c $(NVCCFLAGS) -o $@ $<

# linking
merge:
	@echo linking $@
	$(LD) -o $@ $^ $(LFLAGS)

clean:
	rm -f *.o $(PROJ)

# DEPENDENCIES
merge: merge.o
merge.o: merge.cu
