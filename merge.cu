#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <cuda.h>
#include <helper_cuda.h>

#include "merge.h"

#define M 1000
#define N 800
#define THREADS_PER_BLOCK 512

int main() {
  // Matrices on the host
  double *A, *R;
  A = (double *)malloc(M*N * sizeof(double));
  R = (double *)malloc(N*N * sizeof(double));

  /**
    * Let's populate the A matrix, following this rule:
    * A(ii, ii) = ii + 1; for ii = 0, N − 1
    */
  int ii;
  for (ii = 0; ii < N; ii++) {
    A[ii + ii*N] = (double)(ii + 1);
  }

  // Gram will take care of calculating the factorization
  gram(A, M, N, R);

  // We have finished, let's clean
  free(A); free(R);

  return 0;
}

void gram(double *A, int m, int n, double *R) {
  if (m < n) {
    printf("[!] gram function: m has to be >= n\n");
    return;
  }

  // Performance support vars
  float elapsedTime, overallTime;
  double res = (double)M;
  cudaEvent_t start, overall_start, stop;

  /**
    * Matrices on the device.
    * In *A_d we copy A, *R_d is void
    */
  double *A_d, *R_d;

  cudaEventCreate(&overall_start);
  cudaEventRecord(overall_start, 0);

  checkCudaErrors(cudaMalloc((void **) &A_d, M*N * sizeof(double)));
  checkCudaErrors(cudaMalloc((void **) &R_d, N*N * sizeof(double)));
  checkCudaErrors(cudaMemcpy(A_d, A, M*N * sizeof(double), cudaMemcpyHostToDevice));

  cudaEventCreate(&start);
  cudaEventCreate(&stop);
  cudaEventRecord(start, 0);

  int ii;
  double s_h;

  dim3 dimBlock(THREADS_PER_BLOCK, 1, 1);

  for (ii = 0; ii < n; ii++) {
    // Step 1, call XTA
    xTA_kernel <<< n-ii, dimBlock >>> (&(R_d[ii*n + ii]), n-ii, &(A_d[ii]), m, n, &(A_d[ii]), n);

    // Step 2, we need a single element of R_d, so we copy it and calculate the sqrt
    checkCudaErrors(cudaMemcpy(&s_h, &(R_d[ii*n + ii]), sizeof(double), cudaMemcpyDeviceToHost));
    s_h = sqrt(s_h);

    // Step 3 scale for A_d
    scale_kernel <<< m, dimBlock >>> (&(A_d[ii]), m, n, s_h);

    // Step 4 scale for R_d
    scale_kernel <<< n-ii, dimBlock >>> (&(R_d[ii*n + ii]), n-ii, 1, s_h);

    // Step 5 r1_update
    r1_update_kernel <<< m, dimBlock >>> (&(A_d[ii+1]), m, n-ii-2, n, &(A_d[ii]), n, &(R_d[ii]));
  }

  // We calculate the performances
  cudaEventRecord(stop, 0);
  cudaEventSynchronize(stop);

  cudaEventElapsedTime(&elapsedTime, start, stop);
  cudaEventElapsedTime(&overallTime, overall_start, stop);

  // Let's try with only CPU now
  clock_t start_cpu = clock();
  double s;

  for (ii = 0; ii < n; ii++) {
    // Step 1, call XTA
    xTA(&(R[ii*n + ii]), n-ii, &(A[ii]), m, n, &(A[ii]), n);

    // Step 2, we calculate the scale factor
    s = sqrt(R[ii*n + ii]);

    // Step 3, scale for A
    scale(&(A[ii]), m, n, s);

    // Step 4, scale for R
    scale(&(R[ii*n + ii]), n-ii, 1, s);

    // Step 5, r1_update
    r1_update(&(A[ii+1]), m, n-ii-2, n, &(A[ii]), n, &(R[ii]));
  }

  float elapsedCPU = ((clock() - start_cpu) / (float) CLOCKS_PER_SEC*1000.0);

  printf("Errore: %e \n", fabs(A[0] - res) / fabs(res));
  printf("Tempo CPU: %12.3f [ms].\n", elapsedCPU);
  printf("Tempo GPU %12.3f [ms], speedup: %3.3f.\n", elapsedTime, elapsedCPU/elapsedTime);
  printf("Tempo GPU + copia: % 12.3f [ms], speedup: %3.3f.\n", overallTime, elapsedCPU/overallTime);
  printf("Banda di processamento: %6.2f GB/s\n", m*n*sizeof(double) / elapsedTime*1e-6);

  cudaEventDestroy(start);
  cudaEventDestroy(overall_start);
  cudaEventDestroy(stop);

  // And we're done,
  cudaFree(A_d); cudaFree(R_d);
}

void xTA (double *y, int k, double *A, int m, int lda, double *x, int ldx){
  int ii, jj;
  double sum;

  // The result has to be saved in y
  for (ii = 0; ii < k; ii++) {
    sum = 0;
    for (jj = 0; jj < m; jj++) {
      sum += x[jj*ldx] * A[ii + jj*lda];
    }
    y[ii] = sum;
  }
}

__global__ void xTA_kernel(double *y, int k, double *A, int m, int lda, double *x, int ldx) {
  unsigned int idx = blockIdx.x * blockDim.x + threadIdx.x;
  int ii;
  double sum = 0;

  // Each thread calculates one element of the result
  if (idx < k) {
    for (ii = 0; ii < m; ii++) {
      sum += x[ii*ldx] * A[idx + ii*lda];
    }
    y[idx] = sum;
  }
}

void scale(double *d, int m, int ld, double s) {
  int ii;
  if (s == 0) return; // Just to be sure

  /**
    * We multiplicate each element of the row or the column for the scalar
    * element passed as argument
    */
  for (ii = 0; ii < m; ii++) {
    d[ld *ii] /= s;
  }
}

__global__ void scale_kernel(double *d, int m, int ld, double s) {
  unsigned int idx = blockIdx.x * blockDim.x + threadIdx.x;

  // Each thread scales one element
  if (idx < m && s != 0) {
    d[ld * idx] /= s;
  }
}

void r1_update(double *A, int m, int n, int lda, double *col, int ldc, double *row) {
  int ii, jj;

  for (ii = 0; ii < m; ii++) {
    for (jj = 0; jj < n; jj++) {
      /**
        * Each row of a is updated by a single update of col combined with
        * differents elements of row
        */
      A[ii*lda + jj] -= col[ii*ldc] * row[jj];
    }
  }
}

__global__ void r1_update_kernel(double *A, int m, int n, int lda, double *col, int ldc, double *row) {
  unsigned int idx = blockIdx.x * blockDim.x + threadIdx.x;
  int ii;

  if (idx < m) {
    // Each thread updates one row
    for (ii = 0; ii < n; ii++) {
      A[idx*lda + ii] -= col[idx*ldc] * row[ii];
    }
  }
}
