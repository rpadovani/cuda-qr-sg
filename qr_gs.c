#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#include "qr_gs.h"

#define M 1000
#define N 800

int main() {
  double *A, *R;
  A = (double *)malloc(M*N * sizeof(double));
  R = (double *)malloc(N*N * sizeof(double));

  /**
    * Let's populate the A matrix, following this rule:
    * A(ii, ii) = ii + 1; for ii = 0, N − 1
    */
  int ii;
  for (ii = 0; ii < N; ii++) {
    A[ii + ii*N] = (double)(ii + 1);
  }

  // Gram will take care of calculating the factorization
  gram(A, M, N, R);

  // We have finished, let's clean
  free(A); free(R);

  return 0;
}

void gram(double *A, int m, int n, double *R) {
  if (m < n) {
    printf("[!] gram function: m has to be >= n\n");
    return;
  }

  int ii;
  double s;

  clock_t start_cpu = clock();

  for (ii = 0; ii < n; ii++) {
    // Step 1, call XTA
    xTA(&(R[ii*n + ii]), n-ii, &(A[ii]), m, n, &(A[ii]), n);

    // Step 2, we calculate the scale factor
    s = sqrt(R[ii*n + ii]);

    // Step 3, scale for A
    scale(&(A[ii]), m, n, s);

    // Step 4, scale for R
    scale(&(R[ii*n + ii]), n-ii, 1, s);

    // Step 5, r1_update
    r1_update(&(A[ii+1]), m, n-ii-2, n, &(A[ii]), n, &(R[ii]));
  }

  float elapsedCPU = ((clock() - start_cpu) / (float) CLOCKS_PER_SEC*1000.0f );

  printf("Tempo CPU: %12.3f [ms]\n", elapsedCPU);
}

void xTA (double *y, int k, double *A, int m, int lda, double *x, int ldx){
  int ii, jj;
  double sum;

  // The result has to be saved in y
  for (ii = 0; ii < k; ii++) {
    sum = 0;
    for (jj = 0; jj < m; jj++) {
      sum += x[jj*ldx] * A[ii + jj*lda];
    }
    y[ii] = sum;
  }
}

void scale(double *d, int m, int ld, double s) {
  int ii;
  if (s == 0) return; // Just to be sure

  /**
    * We multiplicate each element of the row or the column for the scalar
    * element passed as argument
    */
  for (ii = 0; ii < m; ii++) {
    d[ld *ii] /= s;
  }
}

void r1_update(double *A, int m, int n, int lda, double *col, int ldc, double *row) {
  int ii, jj;

  for (ii = 0; ii < m; ii++) {
    for (jj = 0; jj < n; jj++) {
      /**
        * Each row of a is updated by a single update of col combined with
        * differents elements of row
        */
      A[ii*lda + jj] -= col[ii*ldc] * row[jj];
    }
  }
}
