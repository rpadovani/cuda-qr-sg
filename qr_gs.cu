#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <cuda.h>
#include <helper_cuda.h>

#include "qr_gs.h"

#define M 1000
#define N 800
#define THREADS_PER_BLOCK 512

/**
  * These functions work like the ones described in qr_gs.h, they're declared
  * as __global__ to work on the device
  */
__global__ void xTA_kernel(double *y, int k, double *A, int m, int lda, double *x, int ldx);
__global__ void scale_kernel(double *d, int m, int ld, double s);
__global__ void r1_update_kernel(double *A, int m, int n, int lda, double *col, int ldc, double *row);

int main() {
  // Matrices on the host
  double *A, *R;
  A = (double *)malloc(M*N * sizeof(double));
  R = (double *)malloc(N*N * sizeof(double));

  /**
    * Let's populate the A matrix, following this rule:
    * A(ii, ii) = ii + 1; for ii = 0, N − 1
    */
  int ii;
  for (ii = 0; ii < N; ii++) {
    A[ii + ii*N] = (double)(ii + 1);
  }

  // Gram will take care of calculating the factorization
  gram(A, M, N, R);

  // We have finished, let's clean
  free(A); free(R);

  return 0;
}

void gram(double *A, int m, int n, double *R) {
  if (m < n) {
    printf("[!] gram function: m has to be >= n\n");
    return;
  }

  // Performance support vars
  float elapsedTime;
  double res = (double)M;
  cudaEvent_t start, stop;

  /**
    * Matrices on the device.
    * In *A_d we copy A, *R_d is void
    */
  double *A_d, *R_d;

  checkCudaErrors(cudaMalloc((void **) &A_d, M*N * sizeof(double)));
  checkCudaErrors(cudaMalloc((void **) &R_d, N*N * sizeof(double)));
  checkCudaErrors(cudaMemcpy(A_d, A, M*N * sizeof(double), cudaMemcpyHostToDevice));

  cudaEventCreate(&start);
  cudaEventCreate(&stop);
  cudaEventRecord(start, 0);

  int ii;
  double s_h;

  dim3 dimBlock(THREADS_PER_BLOCK, 1, 1);

  for (ii = 0; ii < n; ii++) {
    // Step 1, call XTA
    xTA_kernel <<< n-ii, dimBlock >>> (&(R_d[ii*n + ii]), n-ii, &(A_d[ii]), m, n, &(A_d[ii]), n);

    // Step 2, we need a single element of R_d, so we copy it and calculate the sqrt
    checkCudaErrors(cudaMemcpy(&s_h, &(R_d[ii*n + ii]), sizeof(double), cudaMemcpyDeviceToHost));
    s_h = sqrt(s_h);

    // Step 3 scale for A_d
    scale_kernel <<< m, dimBlock >>> (&(A_d[ii]), m, n, s_h);

    // Step 4 scale for R_d
    scale_kernel <<< n-ii, dimBlock >>> (&(R_d[ii*n + ii]), n-ii, 1, s_h);

    // Step 5 r1_update
    r1_update_kernel <<< m, dimBlock >>> (&(A_d[ii+1]), m, n-ii-2, n, &(A_d[ii]), n, &(R_d[ii]));
  }

  // We copy the results back to host
  checkCudaErrors(cudaMemcpy(A, A_d, M*N * sizeof(double), cudaMemcpyDeviceToHost));
  checkCudaErrors(cudaMemcpy(R, R_d, N*N * sizeof(double), cudaMemcpyDeviceToHost));

  // We calculate the performances
  cudaEventRecord(stop, 0);
  cudaEventSynchronize(stop);

  cudaEventElapsedTime(&elapsedTime, start, stop);

  printf("Errore: %e \n", fabs(A[0] - res) / fabs(res));
  printf("Tempo GPU %12.3f [ms]\n", elapsedTime);
  printf("Banda di processamento: %6.2f GB/s\n", m*n*sizeof(double) / elapsedTime*1e-6);

  cudaEventDestroy(start);
  cudaEventDestroy(stop);

  // And we're done,
  cudaFree(A_d); cudaFree(R_d);
}

__global__ void xTA_kernel(double *y, int k, double *A, int m, int lda, double *x, int ldx) {
  unsigned int idx = blockIdx.x * blockDim.x + threadIdx.x;
  int ii;
  double sum = 0;

  // Each thread calculates one element of the result
  if (idx < k) {
    for (ii = 0; ii < m; ii++) {
      sum += x[ii*ldx] * A[idx + ii*lda];
    }
    y[idx] = sum;
  }
}

__global__ void scale_kernel(double *d, int m, int ld, double s) {
  unsigned int idx = blockIdx.x * blockDim.x + threadIdx.x;

  // Each thread scales one element
  if (idx < m && s != 0) {
    d[ld * idx] /= s;
  }
}

__global__ void r1_update_kernel(double *A, int m, int n, int lda, double *col, int ldc, double *row) {
  unsigned int idx = blockIdx.x * blockDim.x + threadIdx.x;
  int ii;

  if (idx < m) {
    // Each thread updates one row
    for (ii = 0; ii < n; ii++) {
      A[idx*lda + ii] -= col[idx*ldc] * row[ii];
    }
  }
}
