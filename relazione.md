# Fattorizzazione QR: algoritmo di Gram-Schmidt modificato

Riccardo Padovani, Matricola 115509

Esame di Algoritmi per il calcolo parallelo, I semestre AA '15/'16

## Obiettivo

Implementare l'algoritmo di Gram-Schmidt modificato per la fattorizzazione di
matrici QR, prima in C usando solo le librerie standard, poi usando il framework
CUDA.

Confrontare i tempi di elaborazione con matrici di dimensioni diverse (in
particolare, 400x300 e 1000x800) e calcolare i relativi tempi di speedup.

## Approccio

Per prima cosa mi sono dedicato alla scrittura del programma per la
fattorizzazione in C (qr_gs.c), procedendo a scrivere una funzione per volta,
una per ogni step, e verificando la corretta implementazione di ognuna.

La maggior difficoltà qua è stata comprendere a fondo cosa rappresenta la
leading dimension in una matrice salvata in un'array. Oltre alla definizione
pratica (la ld è pari alla lunghezza di una riga), le implicazioni pratiche sono
più sottili.

In questo procedimento mi ha aiutato molto lavorare su matrici molto piccole e
scrivere i vari passaggi su carta, in modo da poter poi replicarli al computer.
gdb ha aiutato a debuggare il programma quando andava in stack overflow.

Una volta finalizzato il programma, e usato tools per controllare che
l'implementazione fosse effettivamente corretta (octave e Wolfram Alpha su
tutti), sono passato all'implementazione in CUDA (qr_gs.cu), che è stata
decisamente più semplice.

La logica del programma mi era infatti già chiara, quindi una volta studiata a
fondo la teoria per capire come CUDA sfrutti i threads e i blocchi la
"conversione" delle funzioni da C allo sfruttamento di CUDA è stata molto
lineare.

Verificato che anche il programma in CUDA funzionasse come mi aspettavo, ho
aggiunto le variabili per calcolare le performance, e unito i due file in uno
solo (merge.cu) in modo da poter lasciare al computer il compito di calcolare lo
speedup.

## Analisi dei risultati

Per eseguire il programma ho usato il server tesla messo a disposizione
dall'università.

Per una matrice 400x300 i risultati sono i seguenti:

Tempo CPU:      310.000 [ms].
Tempo GPU      290.271 [ms], speedup: 1.068.
Tempo GPU + copia:      291.014 [ms], speedup: 1.065.
Banda di processamento:  3.307 GB/s

Per una matrice 1000x800 invece questi sono i risultati:

Tempo CPU:     5250.000 [ms].
Tempo GPU     2670.703 [ms], speedup: 1.966.
Tempo GPU + copia:     2672.916 [ms], speedup: 1.964.
Banda di processamento:  2.396 GB/s

Per la matrice 400x300 lo speedup è stato molto relativo: i dati sono
relativamente pochi (120k) quindi la cache della CPU è probabilmente abbastanza
per rendere ancora efficace il calcolo CPU-only.

Al contrario con una matrice decisamente più grande (800k elementi) lo speedup è
davvero notevole, arrivando a un fattore di quasi 2.

È altresì interessante notare come la banda di processamento diminuisca di
molto: essendo molti di più i dati, il bus di comunicazione viene riempito,
rendendo quindi più lento il trasferimento.

## Note finali

Il programma in CUDA può essere probabilmente ottimizzato molto meglio, il mio è
stato una semplice "traduzione" dal programma lineare a uno che sfruttasse la
GPU. Sicuramente esistono tecniche per rendere ancora più performante la
computazione.

CUDA si è dimostrato quindi un ottimo modo per parallelizzare i calcoli, e
aumentare le prestazioni in modo notevole (probabilmente aumentando la grandezza
della matrice lo speedup potrebbe essere ancora maggiore).
